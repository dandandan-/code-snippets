#include "GState_LoadingScreen.h"
#include "ResourceLoader.hpp"
#include "Soundbank.hpp"
#include "GState_MenuSystem.h"
#include "GState_Test.hpp"
#include "MapLoader.hpp"

bool GState_LoadingScreen::m_importantAssetsLoaded = false;
bool GState_LoadingScreen::m_UIAssetsLoaded = false;

GState_LoadingScreen::GState_LoadingScreen( ResourceLoader& resourceLoader, sf::RenderWindow& window, char* key, EGameAreas previousArea, EGameAreas areaToLoad )
	: GameState::GameState( resourceLoader, window, key )
	, m_resourceLoader ( resourceLoader )
	, m_window ( window )
	, m_mainListFilePath ( "Data\\Loading\\LoadMainList.xml" )
	, m_centerHeightPadding ( 50 )
	, m_camera( window )
{	
	// Set the strings
	setFilePaths();

	// Setup the loading text
	setupScreen();

	// Unload resources
	setupUnloadResources( previousArea, areaToLoad );

	// Start loading
	setupLoadResources( areaToLoad );
}


GState_LoadingScreen::~GState_LoadingScreen()
{
	
}

void GState_LoadingScreen::Render()
{
	m_window.clear( sf::Color::Black );
	m_window.draw( m_loadingText );
	m_loadingBar.draw( m_window );
	
#ifdef _DEBUG
	m_window.draw( m_resourceText );
#endif

	m_window.display();
}

void GState_LoadingScreen::exit()
{
	delete this;
}

void GState_LoadingScreen::setFilePaths()
{
	// Load the document
	tinyxml2::XMLDocument mainFile;
	mainFile.LoadFile( m_mainListFilePath.c_str() );

	// Get the current XMLDocument and the root
	tinyxml2::XMLElement* root = mainFile.FirstChildElement( "root" );

	// Loop through the root children
	for ( tinyxml2::XMLElement* currentRootChild = root->FirstChildElement(); currentRootChild; currentRootChild = currentRootChild->NextSiblingElement() )
	{
		// Get the tag name and attribute value
		const std::string& tagName = currentRootChild->Value();
		const std::string& filePath = currentRootChild->FirstAttribute()->Value();

		// Add on the path prefix
		std::string prefix( "Data\\Loading\\" );
		std::string fullPath = prefix + filePath;

		// Create a pair of the name and value and add it to the map 
		std::pair < const std::string, const std::string > _pair( tagName, fullPath );
		m_filePaths.insert( _pair );
	}
}

void GState_LoadingScreen::setupScreen()
{
	// Load the font and set the text font and string
	m_resourceLoader.addFont( "menuFont", "Assets/Fonts/Barkentina.otf" );
	m_loadingFont = *m_resourceLoader.getFont( "menuFont" );
	m_loadingText.setFont( m_loadingFont );
	m_loadingText.setString( "Loading..." );

	// Set the size, colour and origin
	m_loadingText.setCharacterSize( 75 );
	m_loadingText.setFillColor( sf::Color::White );
	m_loadingText.setOutlineColor( sf::Color::Black );
	sf::FloatRect& textBounds = m_loadingText.getLocalBounds();
	m_loadingText.setOrigin( textBounds.width / 2, textBounds.height / 2 );

	// ---- Load bar ---- //

	// Set the colours of the load bar
	m_loadingBar.setFillColor( sf::Color::White );
	m_loadingBar.setOutlineColor( sf::Color::White );

	// Set the size and origins
	m_loadingBar.setSize( sf::Vector2f( textBounds.width, 20.0f ) );

	// Set the positions of the text and load bar
	sf::Vector2u windowSize = m_window.getSize();
	m_loadingText.setPosition( static_cast< float >( windowSize.x / 2 ), static_cast< float >( windowSize.y / 2 ) - m_centerHeightPadding );
	m_loadingBar.setPosition( static_cast< float >( windowSize.x / 2 ), static_cast< float >( windowSize.y / 2 ) + m_centerHeightPadding );

#ifdef _DEBUG

	// Set up the resource text
	m_resourceText.setFont( m_loadingFont );
	m_resourceText.setCharacterSize( 25 );
	m_resourceText.setFillColor( sf::Color::White );
	m_resourceText.setOutlineColor( sf::Color::Black );
	m_resourceText.setPosition( static_cast< float >( windowSize.x / 2 ), static_cast< float >( windowSize.y / 1.2f ) );

#endif
}

void GState_LoadingScreen::setupUnloadResources( EGameAreas previousArea, EGameAreas areaToLoad )
{
	// Return out if none
	if ( previousArea == EGameAreas::ENONE )
		return;

	// Set the chosen document
	std::string chosenDocument;
	setChosenDocument( chosenDocument, previousArea );

	// Create the document
	tinyxml2::XMLDocument document;
	document.LoadFile( chosenDocument.c_str() );

	// Create the maps and add them to the array
	std::map < const std::string, const std::string >* mapPairArray[ EResourceType::ENUMOFRESOURCETYPES ];
	std::map < const std::string, const std::string	> textureMapPairs;
	std::map < const std::string, const std::string	> soundBufferMapPairs;
	std::map < const std::string, const std::string	> musicMapPairs;
	std::map < const std::string, const std::string	> fontMapPairs;
	std::map < const std::string, const std::string > tilesetPairs;
	mapPairArray[ EResourceType::ETEXTURE	] = &textureMapPairs;
	mapPairArray[ EResourceType::ESOUND		] = &soundBufferMapPairs;
	mapPairArray[ EResourceType::EMUSIC		] = &musicMapPairs;
	mapPairArray[ EResourceType::EFONT		] = &fontMapPairs;
	mapPairArray[ EResourceType::ETILESET	] = &tilesetPairs;

	// Get the number of resources
	int amountOfAssets = 0;

	// Check for UI loading
	if ( m_UIAssetsLoaded )
	{
		// If the area loading is the menu, unload ingame UI
		if ( areaToLoad == EGameAreas::EMENU )
		{
			// Change the document to the UI list
			chosenDocument = m_filePaths.at( "UI" );
			document.LoadFile( chosenDocument.c_str() );

			// Get the assets from the list
			getAssetsFromFile( document, mapPairArray, amountOfAssets );

			// Change it back to what it was before
			setChosenDocument( chosenDocument, areaToLoad );
			document.LoadFile( chosenDocument.c_str() );

			// Set UI assets loaded to true
			m_UIAssetsLoaded = true;
		}
	}

	// Read the file
	getAssetsFromFile( document, mapPairArray, amountOfAssets );

	// Unload the resources
	unloadResources( mapPairArray, amountOfAssets );
}

void GState_LoadingScreen::unloadResources( std::map < const std::string, const std::string >* mapPairArray[ EResourceType::ENUMOFRESOURCETYPES ], int& amountOfAssets )
{
	// Loop through each asset list
	for ( int i = 0; i < EResourceType::ENUMOFRESOURCETYPES; i++ )
	{
		// Get the end of the current list
		std::map<const std::string, const std::string>::iterator end = mapPairArray[ i ]->end();

		// Loop through the current list
		for ( std::map<const std::string, const std::string>::iterator itr = mapPairArray[ i ]->begin(); itr != end; ++itr )
		{
			switch ( i )
			{
				case EResourceType::ETEXTURE:
					m_resourceLoader.deleteTexture( itr->first );
					break;

				case EResourceType::ESOUND:
					m_resourceLoader.deleteSoundBuffer( itr->first );
					Soundbank::getSoundbank()->removeSound( itr->first );
					break;

				case EResourceType::EMUSIC:
					m_resourceLoader.deleteMusic( itr->first );
					break;
				
				case EResourceType::EFONT:
					m_resourceLoader.deleteFont( itr->first );
					break;

				case EResourceType::ETILESET:
					m_resourceLoader.deleteTileset( itr->first );
					break;
			}
		}
	}
}

void GState_LoadingScreen::setupLoadResources( EGameAreas areaToLoad )
{
	// The chosen document
	std::string chosenDocument;
	setChosenDocument( chosenDocument, areaToLoad );
	
	// Create and load the document
	tinyxml2::XMLDocument document;
	document.LoadFile( chosenDocument.c_str() );

	// Create the maps and put them in an array for easier parsing
	std::map < const std::string, const std::string >* mapPairArray[ EResourceType::ENUMOFRESOURCETYPES ];
	std::map < const std::string, const std::string	> textureMapPairs;
	std::map < const std::string, const std::string	> soundBufferMapPairs;
	std::map < const std::string, const std::string	> musicMapPairs;
	std::map < const std::string, const std::string	> fontMapPairs;
	std::map < const std::string, const std::string > tilesetPairs;
	mapPairArray[ EResourceType::ETEXTURE	] = &textureMapPairs;
	mapPairArray[ EResourceType::ESOUND		] = &soundBufferMapPairs;
	mapPairArray[ EResourceType::EMUSIC		] = &musicMapPairs;
	mapPairArray[ EResourceType::EFONT		] = &fontMapPairs;
	mapPairArray[ EResourceType::ETILESET	] = &tilesetPairs;

	// Get the number of resources
	int amountOfAssets = 0;

	// Load important stuff if it hasn't been loaded before
	if ( !m_importantAssetsLoaded )
	{
		// Change the document to the important asset list
		setChosenDocument( chosenDocument, EGameAreas::ENONE );
		document.LoadFile( chosenDocument.c_str() );

		// Get the assets from the list
		getAssetsFromFile( document, mapPairArray, amountOfAssets );

		// Change it back to what it was before
		setChosenDocument( chosenDocument, areaToLoad );
		document.LoadFile( chosenDocument.c_str() );

		// Set important assets loaded to true
		m_importantAssetsLoaded = true;
	}

	// Check for UI loading
	if ( !m_UIAssetsLoaded )
	{
		// If the area is in game, load the UI
		if ( areaToLoad != EGameAreas::EMENU && areaToLoad != EGameAreas::ENONE )
		{
			// Change the document to the UI list
			chosenDocument = m_filePaths.at( "UI" );
			document.LoadFile( chosenDocument.c_str() );

			// Get the assets from the list
			getAssetsFromFile( document, mapPairArray, amountOfAssets );

			// Change it back to what it was before
			setChosenDocument( chosenDocument, areaToLoad );
			document.LoadFile( chosenDocument.c_str() );

			// Set UI assets loaded to true
			m_UIAssetsLoaded = true;
		}
	}

	// Read the file
	getAssetsFromFile( document, mapPairArray, amountOfAssets );

	// Load the resources
	loadResources( mapPairArray, amountOfAssets, areaToLoad );
}

void GState_LoadingScreen::loadResources( std::map < const std::string, const std::string >* mapPairArray[ EResourceType::ENUMOFRESOURCETYPES ], int& amountOfAssets, EGameAreas areaToLoad )
{
	int completedAssets = 0;

	// Loop through each asset list
	for ( int i = 0; i < EResourceType::ENUMOFRESOURCETYPES; i++ )
	{
		// Get the end of the current list
		std::map<const std::string, const std::string>::iterator end = mapPairArray[ i ]->end();

		// Loop through the current list
		for ( std::map<const std::string, const std::string>::iterator itr = mapPairArray[ i ]->begin(); itr != end; ++itr )
		{
#ifdef _DEBUG
			// Change the resource text
			setResourceText( std::string( "Loading: " + itr->second ) );
#endif
			switch ( i )
			{
				case EResourceType::ETEXTURE:
					m_resourceLoader.addTexture( itr->first, itr->second );
					break;

				case EResourceType::ESOUND:
					m_resourceLoader.addSoundBuffer( itr->first, itr->second );
					Soundbank::getSoundbank()->createSound( itr->first, m_resourceLoader.getSoundBuffer( itr->first ) );
					break;

				case EResourceType::EMUSIC:
					m_resourceLoader.addMusic( itr->first, itr->second );
					break;

				case EResourceType::EFONT:
					m_resourceLoader.addFont( itr->first, itr->second );
					break;

				case EResourceType::ETILESET:
					MapLoader::getTileset( itr->second.c_str(), m_resourceLoader );
					break;
			}

			// Add to the completed assets
			completedAssets++;

			// Update the load bar
			m_loadingBar.updateBar( static_cast< float >( amountOfAssets ), static_cast< float >( completedAssets ) );

			// Render to screen
			Render();
		}
	}

	// Finish loading
	finishLoading( areaToLoad );

	// Update the bar to 100%
	m_loadingBar.updateBar( static_cast< float >( amountOfAssets ), static_cast< float >( amountOfAssets ) );

#ifdef _DEBUG
	setResourceText( std::string( "Creating objects.." ) );
#endif

	Render();
}

void GState_LoadingScreen::setChosenDocument( std::string& chosenDocument, EGameAreas gameArea )
{
	switch ( gameArea )
	{
		case EGameAreas::EMENU:
			chosenDocument = m_filePaths.at( "menu" );
			break;

		case EGameAreas::ETEST:
			chosenDocument = m_filePaths.at( "villageArea" );
			break;

		case EGameAreas::EVILLAGE:
			chosenDocument = m_filePaths.at( "villageArea" );
			break;

		case EGameAreas::EFOREST:
			chosenDocument = m_filePaths.at( "forestArea" );
			break;

		case EGameAreas::ENONE:
			// Set the none case to cater for the important assets
			chosenDocument = m_filePaths.at( "importantAssets" );
			break;
	}
}

void GState_LoadingScreen::getAssetsFromFile( tinyxml2::XMLDocument& document, std::map < const std::string, const std::string >* mapPairArray[ EResourceType::ENUMOFRESOURCETYPES ], int& amountOfAssets )
{
	// Get to the resources node
	tinyxml2::XMLElement* resources = document.FirstChildElement()->FirstChildElement();

	// The resource type 
	int resourceType = 0;

	// Loop through each of the children of the resources node
	for ( tinyxml2::XMLNode* currentResourceType = resources->FirstChildElement(); currentResourceType; currentResourceType = currentResourceType->NextSiblingElement() )
	{
		// Loop through the individual assets
		for ( tinyxml2::XMLElement* currentAsset = currentResourceType->FirstChildElement(); currentAsset; currentAsset = currentAsset->NextSiblingElement() )
		{
			// Get the key and file path
			const tinyxml2::XMLAttribute* attribute = currentAsset->FirstAttribute();
			const std::string& key = attribute->Value();
			const std::string& filePath = attribute->Next()->Value();

			// Make a pair
			std::pair < const std::string, const std::string > pair( key, filePath );

			// Add the asset to the correct list
			mapPairArray[ resourceType ]->insert( pair );

			// Increase the amount of assets
			amountOfAssets++;
		}

		// Increment the resource type
		resourceType++;

	}
}

void GState_LoadingScreen::finishLoading( EGameAreas areaToLoad )
{
	setStateChange( true );
	
	switch ( areaToLoad )
	{
		case EGameAreas::EMENU:
			setNextState( new GState_MenuSystem( m_resourceLoader, m_window, "Menu" ) );
			break;
		case EGameAreas::EVILLAGE:
			setNextState( new GState_Test( m_resourceLoader, m_window, "Test" ) );
			break;
		case EGameAreas::EFOREST:
			setNextState( new GState_Test( m_resourceLoader, m_window, "Test" ) );
			break;
		case EGameAreas::ETEST:
			setNextState( new GState_Test( m_resourceLoader, m_window, "Test" ) );
			break;

		case EGameAreas::ENONE:
			break;
		default:
			break;
	}
}
#ifdef _DEBUG
void GState_LoadingScreen::setResourceText( const std::string& text )
{
	m_resourceText.setString( text );
	sf::FloatRect& textBounds = m_resourceText.getLocalBounds();
	m_resourceText.setOrigin( textBounds.width / 2, textBounds.height / 2 );
}
#endif // _DEBUG

