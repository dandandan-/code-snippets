﻿using System.Collections.Generic;
using UnityEngine;

public class StateManager : MonoBehaviour {

	// Singleton
	static public StateManager instance { get { return s_Instance; } }
	static protected StateManager s_Instance;

	// State array and getter for the top state
	public State[] states;
	public State topState { get { if ( m_stateList.Count == 0 ) return null; return m_stateList[ m_stateList.Count - 1 ]; } }

	// A list and dictionary of states
	protected List<State> m_stateList = new List<State>();
	protected Dictionary<string, State> m_StateDict = new Dictionary<string, State>();

	protected void OnEnable ()
	{
		// Set the singleton
		s_Instance = this;
		
		// Clear the dictionary
		m_StateDict.Clear();

		// Check for states and add them to the dictionary
		if ( states.Length == 0 )
			return;

		for ( int i = 0; i < states.Length; ++i )
		{
			states[ i ].m_stateManager = this;
			m_StateDict.Add( states[ i ].getName(), states[ i ] );
		}

		// Add them to the list
		m_stateList.Clear();
		pushState( states[ 0 ].getName() );
	}

	/// <summary>
	/// Runs the update of the top state.
	/// </summary>
	protected void Update ()
	{
		if ( m_stateList.Count > 0 )
		{
			m_stateList[ m_stateList.Count - 1 ].update();
		}
	}

	/// <summary>
	/// Switches to a new state.
	/// </summary>
	/// <param name="newState">State to switch to.</param>
	public void switchState ( string newState )
	{
		State state = findState( newState );
		if ( state == null )
		{
			Debug.LogError( "Can't find state: " + newState );
			return;
		}

		// Exit the current state, and enter the next one
		m_stateList[ m_stateList.Count - 1 ].exit( state );
		state.enter( m_stateList[ m_stateList.Count - 1 ] );

		// Remove the old state and add the new one
		m_stateList.RemoveAt( m_stateList.Count - 1 );
		m_stateList.Add( state );
	}

	/// <summary>
	/// Find a state in the state list.
	/// </summary>
	/// <param name="stateName">State to find.</param>
	/// <returns></returns>
	public State findState ( string stateName )
	{
		State state;
		if ( !m_StateDict.TryGetValue( stateName, out state ) )
		{
			Debug.Log( "Can't find state: " + stateName );
			return null;
		}

		return state;
	}

	/// <summary>
	/// Pops (removes) the current state.
	/// </summary>
	public void popState ()
	{
		if ( m_stateList.Count < 2 )
		{
			Debug.LogError( "Can't pop states, only one in stack." );
			return;
		}

		m_stateList[ m_stateList.Count - 1 ].exit( m_stateList[ m_stateList.Count - 2 ] );
		m_stateList[ m_stateList.Count - 2 ].enter( m_stateList[ m_stateList.Count - 2 ] );
		m_stateList.RemoveAt( m_stateList.Count - 1 );
	}

	
	/// <summary>
	/// Push a state onto the stack.
	/// </summary>
	/// <param name="name">The state to push.</param>
	public void pushState ( string name )
	{
		// Attempt to try to find the state, return out if it doesn't find it
		State state = findState( name );

		if ( !state )
			return;
		
		// If there's already a state in the list, run the exit code, then enter the new state
		if ( m_stateList.Count > 0 )
		{
			m_stateList[ m_stateList.Count - 1 ].exit( state );
			state.enter( m_stateList[ m_stateList.Count - 1 ] );
		}
		else
		{
			state.enter( null );
		}

		m_stateList.Add( state );
	}
}
