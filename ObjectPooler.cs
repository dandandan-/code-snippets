﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler {

	/// <summary>
	/// A stack of the free instances.
	/// </summary>
	protected Stack<GameObject> m_freeInstances = new Stack<GameObject>();

	/// <summary>
	/// The original object (the object to pool).
	/// </summary>
	protected GameObject m_originalObject;

	/// <summary>
	/// Creates an object pool with a default size of 20.
	/// </summary>
	public ObjectPooler ( GameObject objectToPool )
	{
		int defaultSize = 20;
		m_originalObject = objectToPool;
		m_freeInstances = new Stack<GameObject>( defaultSize );

		for ( int i = 0; i < defaultSize; ++i )
		{
			GameObject obj = Object.Instantiate( objectToPool );
			obj.SetActive( false );
			m_freeInstances.Push( obj );
		}
	}

	/// <summary>
	/// Creates an object pool with a specified size.
	/// </summary>
	/// <param name="objectToPool">Object to pool.</param>
	/// <param name="initialSize">The size of the pool.</param>
	public ObjectPooler ( GameObject objectToPool, int initialSize )
	{
		m_originalObject = objectToPool;
		m_freeInstances = new Stack<GameObject>( initialSize );

		for ( int i = 0; i < initialSize; ++i )
		{
			GameObject obj = Object.Instantiate( objectToPool );
			obj.SetActive( false );
			m_freeInstances.Push( obj );
		}
	}

	/// <summary>
	/// Get an instance without specifying a position or rotation - will be positioned at 0, 0, 0.
	/// </summary>
	public GameObject Get ()
	{
		return Get( Vector3.zero, Quaternion.identity );
	}

	/// <summary>
	/// Get an instance and place it at pos, at rotation quat.
	/// </summary>
	/// <param name="pos">Position.</param>
	/// <param name="quat">Rotation.</param>
	/// <returns></returns>
	public GameObject Get ( Vector3 pos, Quaternion quat )
	{
		GameObject ret = m_freeInstances.Count > 0 ? m_freeInstances.Pop() : Object.Instantiate( m_originalObject );

		ret.SetActive( true );
		ret.transform.position = pos;
		ret.transform.rotation = quat;

		return ret;
	}

	/// <summary>
	/// Frees an object back to the pool.
	/// </summary>
	/// <param name="obj">Object to free.</param>
	public void Free ( GameObject obj )
	{
		obj.transform.SetParent( null );
		obj.SetActive( false );
		m_freeInstances.Push( obj );
	}
}
