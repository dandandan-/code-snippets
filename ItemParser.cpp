#include "ItemParser.h"
#include "ItemDatabase.h"
#include "tinyxml2.h"
#include "ResourceLoader.hpp"

#ifdef _DEBUG
#include "Debug.cpp"
#endif

ItemParser::ItemParser()
	: m_pItemDatabase ( ItemDatabase::getItemDatabaseObject() )
	, m_pResourceLoader ( ResourceLoader::getResourceLoader() )
{
	// Declare the file names here
	char* weaponListFile		= "Data\\Items\\WeaponList.xml";
	char* armourListFile		= "Data\\Items\\ArmourList.xml";
	char* usableItemListFile	= "Data\\Items\\UsableItemList.xml";
	char* shieldListFile		= "Data\\Items\\ShieldList.xml";

	// Put the file names in an array
	char* listFileNames[ ELists::ENUMBEROFLISTS ] = { weaponListFile, armourListFile, usableItemListFile, shieldListFile };

	// Load the files
	m_XMLWeaponList.LoadFile( weaponListFile );
	m_XMLArmourList.LoadFile( armourListFile );
	m_XMLUsableItemList.LoadFile( usableItemListFile );
	m_XMLShieldList.LoadFile( shieldListFile );

	// Add all the main lists to an array
	m_equipmentListArray[ EGenericItemType::EWEAPON ] = &m_XMLWeaponList;
	m_equipmentListArray[ EGenericItemType::EARMOUR ] = &m_XMLArmourList;
	m_equipmentListArray[ EGenericItemType::EUSABLE ] = &m_XMLUsableItemList;
	m_equipmentListArray[ EGenericItemType::ESHIELD ] = &m_XMLShieldList;

	////// DEBUG BELOW ///////
	std::string debugString;

	// Print whether the file loaded or not
#ifdef _DEBUG
	for ( int i = 0; i < ELists::ENUMBEROFLISTS; i++ )
	{
		if ( m_equipmentListArray[ i ]->ErrorID() == 0 )
		{
			debugString = listFileNames[ i ];
			debugString += " loaded correctly! \n";
			DBOUT( debugString.c_str() );
		}
		else
		{
			debugString = listFileNames[ i ];
			debugString += " failed to load! Error code: ";
			debugString += m_equipmentListArray[ i ]->ErrorID();
			debugString += "\n";
			DBOUT( debugString.c_str() );
		}
	}
#endif

	// Check if the database exists
	if ( m_pItemDatabase != nullptr )
	{
		// Load all the items
		loadItems();
	}
	else
	{
#ifdef _DEBUG
		DBOUT( "ItemParser ctor: ItemDatabase doesn't exist" );
#endif
	}
}

ItemParser::~ItemParser() = default;

void ItemParser::loadItems()
{
	// Loop through all the lists we have
	for ( int listNumber = 0; listNumber < ELists::ENUMBEROFLISTS; listNumber++ )
	{
		tinyxml2::XMLDocument* currentDocument = m_equipmentListArray[ listNumber ];

		// Initialise the element counter and set to 0
		int elementCount = 0;

		// Get the current XMLDocument and the root
		tinyxml2::XMLDocument* currentList = m_equipmentListArray[ listNumber ];
		tinyxml2::XMLElement* root = m_equipmentListArray[ listNumber ]->FirstChildElement( "root" );

		// We need to get the different types of root children - such as meleeWeapon & rangedWeapon, or healthPotions & speedPotions
		// Loop through the root children
		for ( tinyxml2::XMLNode* currentRootChild = root->FirstChildElement(); currentRootChild; currentRootChild = currentRootChild->NextSiblingElement() )
		{
			// Loop through the children of the root child and save the count and item
			for ( tinyxml2::XMLElement* _currentChild = currentRootChild->FirstChildElement(); _currentChild; _currentChild = _currentChild->NextSiblingElement() )
			{
				// Create the string and add the current attribute to it
				std::string currentAttribute = m_listRootFilePath;
				currentAttribute += _currentChild->Attribute( "link" );

				// Create the document from the attribute
				tinyxml2::XMLDocument* currentDocument = new tinyxml2::XMLDocument();
				currentDocument->LoadFile( currentAttribute.c_str() );

				// Parse the correct information
				switch ( listNumber )
				{
					case ELists::EWEAPONLIST:
						parseWeaponFromXML( currentDocument );
						break;

					case ELists::EARMOURLIST:
						parseArmourFromXML( currentDocument );
						break;

					case ELists::EUSABLEITEMLIST:
						parseUsableFromXML( currentDocument );
						break;

					case ELists::ESHIELDLIST:
						parseShieldFromXML( currentDocument );
						break;

					default:
						parseWeaponFromXML( currentDocument );
						break;
					}

				elementCount++;
			}
		}

		// Save the number of items in the list
		m_numberOfItemsInList[ listNumber ] = elementCount;

	}
}

void ItemParser::parseWeaponFromXML( const tinyxml2::XMLDocument* document ) const
{
	// Get the child of the root, which is also the type of the weapon (melee, ranged or special)
	const tinyxml2::XMLElement* rootChild = document->FirstChildElement( "root" )->FirstChildElement();
	std::string weaponTypeString = rootChild->Value();

	// Initialise the weapon type
	ESpecificItemType weaponType = ESpecificItemType::EWEAPONMELEE;

	// Check the weapon type string and set the weapon type accordingly
	setWeaponType( weaponTypeString, weaponType );

	// Create all the variables which are needed
	int itemID = 0, itemRarity = 0, itemBuyValue = 0, itemSellValue = 0, itemRange = 0, itemStackable = 0;
	Rangei itemDamageAmountRange;
	Rangef itemRangeAmount;
	std::string itemName, itemDescription, itemSlug;
	sf::Texture itemTexture;

	// Read all the generic information from the document
	getGenericInformationXML( document, itemID, itemRarity, itemName, itemDescription, itemStackable, itemTexture, itemBuyValue, itemSellValue, itemSlug );

	// Set the weapon stats
	setWeaponStats( document, itemDamageAmountRange, itemRangeAmount );

	// Create the weapon and return it
	if ( weaponType == ESpecificItemType::EWEAPONMELEE )
		m_pItemDatabase->addItemToDatabase( new WeaponMelee( itemID, itemRarity, itemName, itemDescription, itemStackable, itemTexture, itemDamageAmountRange, itemRangeAmount, itemBuyValue, itemSellValue, itemSlug ) );
	else if ( weaponType == ESpecificItemType::EWEAPONRANGED )
		m_pItemDatabase->addItemToDatabase( new WeaponRanged( itemID, itemRarity, itemName, itemDescription, itemStackable, itemTexture, itemDamageAmountRange, itemRangeAmount, itemBuyValue, itemSellValue, itemSlug ) );
	else if ( weaponType == ESpecificItemType::EWEAPONSPECIAL )
		m_pItemDatabase->addItemToDatabase( new WeaponSpecial( itemID, itemRarity, itemName, itemDescription, itemStackable, itemTexture, itemDamageAmountRange, itemRangeAmount, itemBuyValue, itemSellValue, itemSlug ) );
}

void ItemParser::parseArmourFromXML( const tinyxml2::XMLDocument* document ) const
{
	// Get the child of the root, which is also the type of the armour (chest, head, leg)
	const tinyxml2::XMLElement* rootChild = document->FirstChildElement( "root" )->FirstChildElement();
	std::string armourTypeString = rootChild->Value();

	// Initialise the armour type
	ESpecificItemType armourType = ESpecificItemType::EARMOURCHEST;

	// Set the armour type
	setArmourType( armourTypeString, armourType );

	// Create all the variables which are needed
	int itemID = 0, itemRarity = 0, itemBuyValue = 0, itemSellValue = 0, itemStackable = 0;
	Rangei itemHealthRange;
	std::string itemName, itemDescription, itemSlug;
	sf::Texture itemTexture;

	// Read all the generic information from the document
	getGenericInformationXML( document, itemID, itemRarity, itemName, itemDescription, itemStackable, itemTexture, itemBuyValue, itemSellValue, itemSlug );

	// Navigate to the stats node
	const tinyxml2::XMLElement* currentElement = rootChild->FirstChildElement( "stats" );

	// Set the armour stats
	setArmourStats( document, itemHealthRange );

	// Create the armour and return it
	if ( armourType == ESpecificItemType::EARMOURCHEST )
		m_pItemDatabase->addItemToDatabase( new ArmourChestpiece( itemID, itemRarity, itemName, itemDescription, itemStackable, itemTexture, itemHealthRange, itemBuyValue, itemSellValue, itemSlug ) );
	else if ( armourType == ESpecificItemType::EARMOURHELMET )
		m_pItemDatabase->addItemToDatabase( new ArmourHelmet( itemID, itemRarity, itemName, itemDescription, itemStackable, itemTexture, itemHealthRange, itemBuyValue, itemSellValue, itemSlug ) );
	else if ( armourType == ESpecificItemType::EARMOURLEGS )
		m_pItemDatabase->addItemToDatabase( new ArmourLegs( itemID, itemRarity, itemName, itemDescription, itemStackable, itemTexture, itemHealthRange, itemBuyValue, itemSellValue, itemSlug ) );
}

void ItemParser::parseUsableFromXML( const tinyxml2::XMLDocument* document ) const
{
	// Get the child of the root, which is also the type of the usable (health potion, speed potion)
	const tinyxml2::XMLElement* _rootChild = document->FirstChildElement( "root" )->FirstChildElement();
	std::string usableTypeString = _rootChild->Value();

	// Initialise the usable and main type
	EUsableType usableType = EUsableType::EPOTION;
	ESpecificItemType specificType = ESpecificItemType::EUSABLEPOTIONHEALTH;

	// Set the usable type
	setUsableType( usableTypeString, usableType, specificType );

	// Create all the variables which are needed
	int itemID = 0, itemRarity = 0, itemBuyValue = 0, itemSellValue = 0, itemStackable = 0,
		itemPower = 0, itemDuration = 0, itemDamage = 0, itemThrowRange = 0, itemAreaOfEffect = 0;
	std::string itemName, itemDescription, itemSlug;
	sf::Texture itemTexture;

	// Read all the generic information from the document
	getGenericInformationXML( document, itemID, itemRarity, itemName, itemDescription, itemStackable, itemTexture, itemBuyValue, itemSellValue, itemSlug );

	// Set the stats
	setUsableStats( document, usableType, itemPower, itemDuration, itemDamage, itemThrowRange, itemAreaOfEffect );

	switch ( usableType )
	{
		case EUsableType::EPOTION:
			// Add the item to the item database
			if ( usableTypeString == "potionHealth" )		m_pItemDatabase->addItemToDatabase( new UsablePotionHealth( itemID, itemRarity, itemName, itemDescription, itemStackable, itemTexture, itemPower, itemBuyValue, itemSellValue, itemSlug ) );
			else if ( usableTypeString == "potionSpeed" )	m_pItemDatabase->addItemToDatabase( new UsablePotionSpeed( itemID, itemRarity, itemName, itemDescription, itemStackable, itemTexture, itemPower, itemDuration, itemBuyValue, itemSellValue, itemSlug ) );
			break;

		case EUsableType::EBOMB:
			// Create the bomb and return it
			m_pItemDatabase->addItemToDatabase( new UsableBomb( itemID, itemRarity, itemName, itemDescription, itemStackable, itemTexture, itemDamage, itemThrowRange, itemAreaOfEffect, itemBuyValue, itemSellValue, itemSlug ) );
			break;
	}
}

void ItemParser::parseShieldFromXML( const tinyxml2::XMLDocument* document ) const
{
	// Get the child of the root
	const tinyxml2::XMLElement* rootChild = document->FirstChildElement( "root" )->FirstChildElement();

	// Create all the variables which are needed
	int itemID = 0, itemRarity = 0, itemBuyValue = 0, itemSellValue = 0, itemStackable = 0;
	Rangei itemHealthRange, itemBlockAmountRange;
	std::string itemName, itemDescription, itemSlug;
	sf::Texture itemTexture;

	// Read all the generic information from the document
	getGenericInformationXML( document, itemID, itemRarity, itemName, itemDescription, itemStackable, itemTexture, itemBuyValue, itemSellValue, itemSlug );

	// Initialise the current element at the stats node
	const tinyxml2::XMLElement* currentElement = rootChild->FirstChildElement( "stats" );

	// Set the shield stats
	setShieldStats( document, itemHealthRange, itemBlockAmountRange );

	// Create the shield
	m_pItemDatabase->addItemToDatabase( new Shield( itemID, itemRarity, itemName, itemDescription, itemStackable, itemTexture, itemHealthRange, itemBlockAmountRange, itemBuyValue, itemSellValue, itemSlug ) );
}

void ItemParser::getGenericInformationXML( const tinyxml2::XMLDocument* document, int& itemDatabaseID, int& rarity, std::string& name, std::string& description, int& stackable, sf::Texture& texture, int& buyValue, int& sellValue, std::string& slug ) const
{
	// Get the itemDatabaseID
	const tinyxml2::XMLElement* currentElement = document->FirstChildElement( "root" )->FirstChildElement()->FirstChildElement( "databaseID" );
	currentElement->QueryIntText( &itemDatabaseID );

	// Get the rarity
	currentElement = currentElement->NextSiblingElement( "rarity" );
	currentElement->QueryIntText( &rarity );

	// Get the name
	currentElement = currentElement->NextSiblingElement( "name" );
	name = currentElement->GetText();

	// Get the description
	currentElement = currentElement->NextSiblingElement( "description" );
	description = currentElement->GetText();

	// Get the stackable node
	currentElement = currentElement->NextSiblingElement( "stackable" );
	currentElement->QueryIntText( &stackable );

	// Get the texture node
	currentElement = currentElement->NextSiblingElement( "textureFile" );
	const char* cTextureFile = currentElement->GetText();

	// Get the slug
	currentElement = currentElement->NextSiblingElement( "slug" );
	slug = currentElement->GetText();

	// Add the texture to the resource manager and load it
	m_pResourceLoader->addTexture( slug, cTextureFile );
	texture = *m_pResourceLoader->getTexture( slug );

	// Navigate to the stats node
	currentElement = currentElement->NextSiblingElement( "stats" );

	// Get the buy value
	currentElement = currentElement->FirstChildElement( "buyValue" );
	currentElement->QueryIntText( &buyValue );

	// Get the sell value
	currentElement = currentElement->NextSiblingElement( "sellValue" );
	currentElement->QueryIntText( &sellValue );
}

void ItemParser::setWeaponType( const std::string& weaponString, ESpecificItemType& weaponType ) const
{
	if ( weaponString == "weaponMelee" )		weaponType = ESpecificItemType::EWEAPONMELEE;
	else if ( weaponString == "weaponRanged" )	weaponType = ESpecificItemType::EWEAPONRANGED;
	else if ( weaponString == "weaponSpecial" )	weaponType = ESpecificItemType::EWEAPONSPECIAL;
}

void ItemParser::setArmourType( const std::string& armourString, ESpecificItemType& armourType ) const
{
	if ( armourString == "armourChest" )		armourType = ESpecificItemType::EARMOURCHEST;
	else if ( armourString == "armourHead" )	armourType = ESpecificItemType::EARMOURHELMET;
	else if ( armourString == "armourLegs" )	armourType = ESpecificItemType::EARMOURLEGS;
}

void ItemParser::setUsableType( const std::string& usableString, EUsableType usableType, ESpecificItemType& specificType ) const
{
	if ( usableString == "potionHealth" )
	{
		usableType = EUsableType::EPOTION;
		specificType = ESpecificItemType::EUSABLEPOTIONHEALTH;
	}
	else if ( usableString == "potionSpeed" )
	{
		usableType = EUsableType::EPOTION;
		specificType = ESpecificItemType::EUSABLEPOTIONSPEED;
	}
	else if ( usableString == "bomb" )
	{
		usableType = EUsableType::EBOMB;
		specificType = ESpecificItemType::EUSABLEBOMB;
	}
}

void ItemParser::setWeaponStats( const tinyxml2::XMLDocument * document, Rangei& damageAmountRange, Rangef& weaponRangeAmount ) const
{
	const tinyxml2::XMLElement* currentElement = document->FirstChildElement( "root" )->FirstChildElement()->FirstChildElement( "stats" );

	int min, max;

	// Get the min damage and max damage
	currentElement = currentElement->FirstChildElement( "minDamage" );
	currentElement->QueryIntText( &min );
	currentElement = currentElement->NextSiblingElement( "maxDamage" );
	currentElement->QueryIntText( &max );
	damageAmountRange.setRange( min, max );

	// Move to the next sibling (range) and get it
	currentElement = currentElement->NextSiblingElement( "minRange" );
	currentElement->QueryIntText( &min );
	currentElement = currentElement->NextSiblingElement( "maxRange" );
	currentElement->QueryIntText( &max );
	weaponRangeAmount.setRange( static_cast< float >( min ), static_cast< float >( max ) );
}

void ItemParser::setArmourStats( const tinyxml2::XMLDocument * document, Rangei & healthRange ) const
{
	const tinyxml2::XMLElement* currentElement = document->FirstChildElement( "root" )->FirstChildElement()->FirstChildElement( "stats" );

	int min, max;

	// Get the min health and max health
	currentElement = currentElement->FirstChildElement( "minHealth" );
	currentElement->QueryIntText( &min );
	currentElement = currentElement->NextSiblingElement( "maxHealth" );
	currentElement->QueryIntText( &max );
	healthRange.setRange( min, max );
}

void ItemParser::setUsableStats( const tinyxml2::XMLDocument* document, EUsableType& usableType, int& itemPower, int& itemDuration, int& itemDamage, int &itemThrowRange, int &itemAreaOfEffect ) const
{
	// Go to the stats node
	const tinyxml2::XMLElement* currentElement = document->FirstChildElement( "root" )->FirstChildElement()->FirstChildElement( "stats" );

	// Read the stats
	if ( usableType == EUsableType::EPOTION )
	{
		// Go to power and save it
		currentElement = currentElement->FirstChildElement();
		currentElement->QueryIntText( &itemPower );

		// Go to duration and save it
		currentElement = currentElement->NextSiblingElement();
		currentElement->QueryIntText( &itemDuration );
	}
	else if ( usableType == EUsableType::EBOMB )
	{
		// Go to damage and save it
		currentElement = currentElement->FirstChildElement();
		currentElement->QueryIntText( &itemDamage );

		// Go to throw range and save it
		currentElement = currentElement->NextSiblingElement();
		currentElement->QueryIntText( &itemThrowRange );

		// Go to area of effect and save it
		currentElement = currentElement->NextSiblingElement();
		currentElement->QueryIntText( &itemAreaOfEffect );
	}
}

void ItemParser::setShieldStats( const tinyxml2::XMLDocument * document, Rangei & healthRange, Rangei & blockAmountRange ) const
{
	const tinyxml2::XMLElement* currentElement = document->FirstChildElement( "root" )->FirstChildElement()->FirstChildElement( "stats" );

	int min, max;

	// Get the minimum and max block
	currentElement = currentElement->FirstChildElement( "minHealth" );
	currentElement->QueryIntText( &min );
	currentElement = currentElement->NextSiblingElement( "maxHealth" );
	currentElement->QueryIntText( &max );
	healthRange.setRange( min, max );

	// Do the same for the block amount
	currentElement = currentElement->NextSiblingElement( "minBlockAmount" );
	currentElement->QueryIntText( &min );
	currentElement = currentElement->NextSiblingElement( "maxBlockAmount" );
	currentElement->QueryIntText( &max );
	blockAmountRange.setRange( min, max );
}