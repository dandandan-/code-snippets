﻿/*
// 	FlyingHeightUpdate.cs
//	Coded by Daniel Seamon.
//
//	This script is responsible for the height of the drone. Also lifts the drone off the floor when the game starts.
//  I tried to make it as easy to use as possible with spawning - you simply place the drones in the editor at the desired height,
//  and they'll automatically snap to the ground and raise to the height over the countdown duration.
//  This version simply uses entering and exiting colliders, rather than raycasting. This seems to have a less smooth approach,
//  but is much more optimized and easier to implement. (~50 less lines, less update checks, etc)
//
*/

using UnityEngine;
using System.Collections;

public class FlyingHeightUpdate : MonoBehaviour
{

    // Height at the spawn - default height of the drones
    private float defaultHeight;

    // Height when on the floor
    private float floorHeight;

    // Height the drone should fly above objects
    private float objectFlyHeightOffset = 4.5f;

    // Current height of the drone
    public float currentHeight;

    // Animation curve of the spawn raise
    public AnimationCurve spawnRaiseCurve;

    // Animation curve of flying up and down
    public AnimationCurve flyHeightCurve;

    // String of the fly over object's tag
    private string flyOverTag = "FlyOver";

    // Whether the drones are raising (spawning)
    private bool spawnRaise;

    // Save the coroutine in a variable
    private IEnumerator adjustHeightCoroutine;

    // Use this for initialization
    void Start()
    {
        // Set the drones to the floor
        SetToFloor();

        // Start the drones flying up to their positions
        StartCoroutine(SpawnRaise());
    }

    // When entering a collider, check if it's a flyover object and raise height
    void OnTriggerEnter(Collider o)
    {
        if (o.tag == flyOverTag)
        {
            GameObject _o = o.gameObject;

            // Get the object's height
            float objectHeight = _o.GetComponent<MeshFilter>().mesh.bounds.extents.y;

            // Multiply by the scale
            objectHeight *= _o.transform.localScale.y;

            // Set the coroutine with the correct parameters in a variable and start it
            adjustHeightCoroutine = AdjustHeight(objectHeight);
            StartCoroutine(adjustHeightCoroutine);
        }
    }

    // Exiting collider
    void OnTriggerExit(Collider o)
    {
        if (o.tag == flyOverTag)
        {
            // Stop the coroutine if it's running
            StopCoroutine(adjustHeightCoroutine);

            // Set the coroutine with the correct parameters in a variable and start it going towards the ground
            adjustHeightCoroutine = AdjustHeight(floorHeight);
            StartCoroutine(adjustHeightCoroutine);
        }
    }

    void FixedUpdate()
    {
        // Set the position of the drone (height only)
        transform.position = new Vector3(transform.position.x, currentHeight, transform.position.z);
    }

    /// <summary>
    /// Sets the drones to floor - run once at the start of the countdown.
    /// </summary>
    private void SetToFloor()
    {
        // Get the height when the drones spawn
        defaultHeight = transform.position.y;

        // Create the ray that will go to the ground
        Ray groundRay = new Ray(transform.position, -transform.up);

        // Create the ray hit (ground)
        RaycastHit objectHit;

        // Move the drone to the floor
        if (Physics.Raycast(groundRay, out objectHit))
        {
            var _o = objectHit;

            // If hit object is the ground
            if (_o.transform.tag == "Ground")
            {
                // Get the new position where the ray intersected
                Vector3 _newPos = _o.point;

                // Set a slight offset on the y
                float _positionOffset = 0.2f;
                _newPos.y += _positionOffset;

                // Set the position
                transform.position = _newPos;

                // Set the floorHeight of the drone at this point
                floorHeight = transform.position.y;
            }
        }
    }

    /// <summary>
    /// Adjusts the height of the drone accordingly.
    /// </summary>
    /// <returns>The height.</returns>
    /// <param name="objectHeight">Object's height.'</param>
    private IEnumerator AdjustHeight(float objectHeight)
    {
        // Get the current height of the drone
        float _currentHeightTemp = transform.position.y;

        // The current offset, dependent on whether we're over ground or an object
        float _currentOffset = defaultHeight;

        // Get the new height, dependent on the object's collider
        float _changedFlyHeight = objectHeight + _currentOffset;

        // Initiate the timer and the time it should take to change
        float _timer = 0f;
        float _changeTime = 0.45f;

        // Change the height
        if (_changedFlyHeight != currentHeight)
        {
            while (_timer < _changeTime)
            {
                // Work out the percentage
                float _percentage = _timer / _changeTime;

                // Change the current height
                currentHeight = Mathf.Lerp(_currentHeightTemp, _changedFlyHeight, flyHeightCurve.Evaluate(_percentage));

                // Increase the timer
                _timer += Time.deltaTime;

                yield return null;
            }
        }

        yield return null;
    }

    /// <summary>
    /// Raises the drones off the ground at spawn over the duration of the countdown.
    /// </summary>
    private IEnumerator SpawnRaise()
    {
        // Set bool to true to stop other functions
        spawnRaise = true;

        // Set the start timer and how long it takes to raise the drones
        float _timer = 0f;
        float _spawnTime = 2.8f;

        while (_timer < _spawnTime)
        {
            // Work out the percentage
            float _percentage = _timer / _spawnTime;

            // Change the current height
            currentHeight = Mathf.Lerp(floorHeight, defaultHeight, spawnRaiseCurve.Evaluate(_percentage));

            // Increase the timer
            _timer += Time.deltaTime;

            yield return null;
        }

        // Set bool to false
        spawnRaise = false;
    }
}
