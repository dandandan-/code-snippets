#include <string.h>
#include "ItemDatabase.h"
#include "tinyxml2.h"
#include "_ItemIncludes.h"
#include "ResourceLoader.hpp"
#include "PlayerItemManager.h"

#ifdef _DEBUG
#include "DEBUG.cpp"
#endif

ItemDatabase* ItemDatabase::m_itemDatabaseObject = nullptr;

ItemDatabase::ItemDatabase( PlayerItemManager* playerItemManager )
	: m_pResourceManager ( ResourceLoader::getResourceLoader() )
	, m_pPlayerItemManager ( playerItemManager )
{
	// Set the pointer to itself
	m_itemDatabaseObject = this;

	// Set up the rarities
	setupRarity();

	// Set up the starter slugs
	m_starterItemSlugs[ 0 ] = "weapon_sword_starter";
	m_starterItemSlugs[ 1 ] = "armour_chest_starter";
	m_starterItemSlugs[ 2 ] = "armour_head_starter";
	m_starterItemSlugs[ 3 ] = "armour_legs_starter";
}

ItemDatabase::~ItemDatabase()
{

}



ItemDatabase* ItemDatabase::getItemDatabaseObject()
{
	return m_itemDatabaseObject;
}

Item* ItemDatabase::getRandomItem( bool addStraightToPlayer )
{
	// Get which rarity should spawn
	EItemRarity itemRarity = getItemSpawnRarity();

	// Get the current list
	std::vector<Item*> currentRarityList = m_pItemRarityLists[ itemRarity ];

	// Set up a range object and randomise which item to get
	Rangei rarityListRange( 0, currentRarityList.size() - 1 );
	int randomItemNum = rarityListRange.getRandomNumberInRange();

	// Get the chosen item
	Item* chosenItem = currentRarityList[ randomItemNum ];
	
	// Check the main type of item it is
	ESpecificItemType specificItemType = chosenItem->getSpecificItemType();
	
	// Check if it's stackable
 	if ( chosenItem->getStackable() )
	{
		// Check if it already exists in the list
  		if ( Item* existingItem = m_pPlayerItemManager->itemExistsInList( chosenItem ) )
		{
			// If it does, simply add a stack of the chosen item
			if ( addStraightToPlayer )
			{
				m_pPlayerItemManager->stackItem( existingItem );

				std::string name = chosenItem->getName();
				std::string displayString = "Item stacked: " + name + ".\n";

				// Output the log
#ifdef _DEBUG
				DBOUT( displayString.c_str() );
#endif

				return existingItem;
			}
		}
	}

	// Create the correct item and copy in the stats from the chosen item
	auto newItem = createItem( specificItemType );
	*newItem = *chosenItem;
	newItem->setPlayerItemManager( m_pPlayerItemManager );
	chosenItem->copyInto( newItem );
	newItem->spawnItem();

	// Add it to the inventory if true 
	if ( addStraightToPlayer )
	{
		m_pPlayerItemManager->addItem( newItem );
	}

	std::string name = newItem->getName();
	std::string displayString = "Item created: " + name + "\n";

	// Output the log
#ifdef _DEBUG
	DBOUT( displayString.c_str() );
#endif

	// Return the new item
	return newItem;
}

Item * ItemDatabase::getSpecificItem( const std::string& slug, const bool addStraightToPlayer )
{
	// Get the item 
	Item* chosenItem = m_itemDatabase.at( slug );

	// Check the main type of item it is and create it
	ESpecificItemType mainItemType = chosenItem->getSpecificItemType();

	// Check if it's stackable
	if ( chosenItem->getStackable() )
	{
		// Check if it already exists in the list
		if ( m_pPlayerItemManager->itemExistsInList( chosenItem ) )
		{
			// If it does, simply add a stack of the chosen item
			if ( addStraightToPlayer )
			{
				m_pPlayerItemManager->addItem( chosenItem );

				std::string name = chosenItem->getName();
				std::string displayString = "Item stacked: " + name + ".\n";

				// Output the log
#ifdef _DEBUG
				DBOUT( displayString.c_str() );
#endif

				return nullptr;
			}
		}
	}

	// Create the correct item and copy in the stats from the chosen item
	auto newItem = createItem( mainItemType );
	*newItem = *chosenItem;
	chosenItem->copyInto( newItem );
	newItem->setPlayerItemManager( m_pPlayerItemManager );
	newItem->spawnItem();

	// Add it to the inventory if true 
	if ( addStraightToPlayer )
	{
		m_pPlayerItemManager->addItem( newItem );
	}

	std::string name = newItem->getName();
	std::string displayString = "Item created: " + name + "\n";

	// Output the log
#ifdef _DEBUG
	DBOUT( displayString.c_str() );
#endif

	return newItem;
}

void ItemDatabase::addItemToDatabase( Item* item )
{
	// Get the slug and rarity
	const std::string& slug = item->getSlug();
	int rarity = item->getRarity();

	// Create a pair of the databaseID and item
	std::pair < const std::string, Item* > _pair = { slug , item };

	// Insert it into the database
	m_itemDatabase.insert( _pair );

	// If it's a starter item, don't add it to the rarity list
	bool starter = false;
	for (const auto & starterItemSlug : m_starterItemSlugs)
	{
		if ( slug == starterItemSlug )
		{
			starter = true;
		}
	}

	// Add it to the correct rarity list
	if ( !starter )
	{
		m_pItemRarityLists[ rarity ].push_back( item );
	}
}

void ItemDatabase::setupRarity()
{
	// Common, rare, epic, mythic
	m_itemRarityChances[ 0 ] = 40;
	m_itemRarityChances[ 1 ] = 30;
	m_itemRarityChances[ 2 ] = 20;
	m_itemRarityChances[ 3 ] = 10;

	// Set the item rarity list array
	m_pItemRarityLists[ 0 ] = m_pCommonItems;
	m_pItemRarityLists[ 1 ] = m_pRareItems;
	m_pItemRarityLists[ 2 ] = m_pEpicItems;
	m_pItemRarityLists[ 3 ] = m_pMythicItems;
}

ItemDatabase::EItemRarity ItemDatabase::getItemSpawnRarity() const
{
	int range = 0;

	for (int itemRarityChance : m_itemRarityChances)
	{
		range += itemRarityChance;
	}

	// Get a random number from the range
	Rangei randRange( 0, range );
	int randNum = randRange.getRandomNumberInRange();

	int topNum = 0;

	// Get the rarity 
	for ( int i = 0; i < EItemRarity::ENUMBEROFRARITIES; i++ )
	{
		topNum += m_itemRarityChances[ i ];
		if ( randNum < topNum )
			return static_cast< EItemRarity >( i );
	}

	return EItemRarity::ECOMMON;
}